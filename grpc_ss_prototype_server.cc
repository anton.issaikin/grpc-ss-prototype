#include <algorithm>
#include <chrono>
#include <iostream>
#include <string>
#include <thread>


#include <grpc/grpc.h>
#include <grpcpp/security/server_credentials.h>
#include <grpcpp/server.h>
#include <grpcpp/server_builder.h>
#include <grpcpp/server_context.h>
#ifdef BAZEL_BUILD
#include "examples/protos/route_guide.grpc.pb.h"
#else
#include "nanny_comm.grpc.pb.h"
#endif

#define PING_INTERVAL 5000

using grpc::CallbackServerContext;
using grpc::Server;
using grpc::ServerBuilder;
using grpc::Status;
using teye::nanny::StreamStatus;
using teye::nanny::StreamConfig;
using teye::nanny::NannyCommunication;
using teye::nanny::StreamConfig;
using teye::nanny::StreamStatus;
using teye::nanny::NotifySummary;
using teye::nanny::ServerId;
using std::chrono::system_clock;

StreamConfig MakeStreamConfig(const std::string& status = "default_name") {
    StreamConfig ss;
    static std::atomic_size_t counter(0);
    ss.set_json_config(std::string("{ \"stream_config\": \"") + std::to_string(counter) + "\"}");
    ss.set_stream_id("some_id_" + std::to_string(counter++));
    //ss.mutable_location()->CopyFrom(MakePoint(latitude, longitude));
    return ss;
}

class RouteGuideImpl final : public NannyCommunication::CallbackService {
 public:
    explicit RouteGuideImpl() {
    }

    grpc::ServerWriteReactor<StreamConfig>* WatchStreamConfig(
            CallbackServerContext* context,
            const teye::nanny::ServerId* server_id) override {
        class Lister : public grpc::ServerWriteReactor<StreamConfig> {
         public:
            Lister(const teye::nanny::ServerId* server_id)
                    : server_id_(server_id->id()) {
                stream_config_ = MakeStreamConfig();
                NextWrite();
            }
            void OnDone() override { delete this; }
            void OnWriteDone(bool /*ok*/) override { NextWrite(); }

         private:
            void NextWrite() {
                while (true) {
                    std::cout << "NextWrite entry point" << std::endl;
                    std::this_thread::sleep_for(std::chrono::seconds(1));
                    stream_config_ = MakeStreamConfig();
                    {
                        StartWrite(&stream_config_);
                        return;
                    }
                }
                // Didn't write anything, all is done.
                Finish(Status::OK);
            }
            std::string server_id_;
            std::vector<StreamConfig> * stream_configs_;
            StreamConfig stream_config_;
        };
        return new Lister(server_id);
    }

    grpc::ServerReadReactor<StreamStatus>* NotifyStreamStatus(CallbackServerContext* context,
                                                NotifySummary* summary) override {
        class Recorder : public grpc::ServerReadReactor<StreamStatus> {
         public:
            Recorder(NotifySummary* summary)
                    : start_time_(system_clock::now()),
                      summary_(summary) {
                StartRead(&stream_status_);
            }
            void OnDone() { delete this; }
            void OnReadDone(bool ok) {
                if (ok) {
                    std::cout << "read_done with: " << stream_status_.stream_id()
                              << " " << stream_status_.json_status() << std::endl;
                    StartRead(&stream_status_);
                } else {
                    summary_->set_notify_uptime((system_clock::now() - start_time_).count());
                    Finish(Status::OK);
                }
            }

         private:
            system_clock::time_point start_time_;
            NotifySummary* summary_;
            StreamStatus stream_status_;
            int feature_count_ = 0;
        };
        return new Recorder(summary);
    }

 private:
    std::mutex mu_;
};

void RunServer() {
    std::string server_address("0.0.0.0:50051");
    RouteGuideImpl service;

    ServerBuilder builder;
    builder.AddChannelArgument(GRPC_ARG_HTTP2_MIN_RECV_PING_INTERVAL_WITHOUT_DATA_MS, PING_INTERVAL);
    builder.AddChannelArgument(GRPC_ARG_HTTP2_MAX_PING_STRIKES, 0);
    builder.AddListeningPort(server_address, grpc::InsecureServerCredentials());
    builder.RegisterService(&service);
    std::unique_ptr<Server> server(builder.BuildAndStart());
    std::cout << "Server listening on " << server_address << std::endl;
    server->Wait();
}

int main(int argc, char** argv) {
    RunServer();

    return 0;
}
