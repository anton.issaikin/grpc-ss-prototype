#include <chrono>
#include <condition_variable>
#include <iostream>
#include <memory>
#include <mutex>
#include <string>
#include <thread>

#include <grpc/grpc.h>
#include <grpcpp/alarm.h>
#include <grpcpp/channel.h>
#include <grpcpp/client_context.h>
#include <grpcpp/create_channel.h>
#include <grpcpp/security/credentials.h>
#ifdef BAZEL_BUILD
#include "examples/protos/route_guide.grpc.pb.h"
#else
#include "nanny_comm.grpc.pb.h"
#endif

#define PING_INTERVAL 5000

using grpc::Channel;
using grpc::ChannelArguments;
using grpc::ClientContext;
using grpc::Status;
using grpc::ClientReadReactor;
using teye::nanny::StreamStatus;
using teye::nanny::StreamConfig;
using teye::nanny::NannyCommunication;
using teye::nanny::StreamConfig;
using teye::nanny::StreamStatus;
using teye::nanny::NotifySummary;
using teye::nanny::ServerId;

StreamStatus MakeStreamStatus(const std::string& status = "default_name") {
    StreamStatus ss;
    static std::atomic_size_t counter(0);
    ss.set_json_status(std::string("{ \"stream_status\": \"") + std::to_string(counter) + "\"}");
    ss.set_stream_id("some_id_" + std::to_string(counter++));
    // ss.mutable_location()->CopyFrom(MakePoint(latitude, longitude));
    return ss;
}


class NannyCommunicationClient {
 public:

    class Reader : public grpc::ClientReadReactor<StreamConfig> {
     public:
        Reader(NannyCommunication::Stub* stub, const ServerId & server_id) {
            stub->async()->WatchStreamConfig(&context_, &server_id, this);
            StartRead(&stream_config_);
            StartCall();
        }
        void OnReadDone(bool ok) override {
            std::cout << " --- OnReadDone thread id " << std::this_thread::get_id() << std::endl;

            if (ok) {
                std::cout << "Got stream config " << stream_config_.stream_id() << " \""
                          << stream_config_.json_config() << "\""
                          << std::endl;
                StartRead(&stream_config_);
            }
        }
        void OnDone(const Status& s) override {
            std::unique_lock<std::mutex> l(mu_);
            status_ = s;
            done_ = true;
            cv_.notify_one();
        }
        Status Await() {
            std::unique_lock<std::mutex> l(mu_);
            cv_.wait(l, [this] { return done_; });
            return std::move(status_);
        }

     private:
        ClientContext context_;
        StreamConfig stream_config_;
        std::mutex mu_;
        std::condition_variable cv_;
        Status status_;
        bool done_ = false;
    };

    class Recorder : public grpc::ClientWriteReactor<StreamStatus> {
     public:
        Recorder(NannyCommunication::Stub* stub) {
            stub->async()->NotifyStreamStatus(&context_, &result_, this);
            // Use a hold since some StartWrites are invoked indirectly from a
            // delayed lambda in OnWriteDone rather than directly from the reaction
            // itself
            AddHold();
            NextWrite();
            StartCall();
        }
        void OnWriteDone(bool ok) override {
            // Delay and then do the next write or WritesDone
            alarm_.Set(
                    std::chrono::system_clock::now() +
                    std::chrono::milliseconds(1000),
                    [this](bool /*ok*/) { NextWrite(); });
        }
        void OnDone(const Status& s) override {
            std::unique_lock<std::mutex> l(mu_);
            status_ = s;
            done_ = true;
            cv_.notify_one();
        }
        Status Await(NotifySummary* stats) {
            std::unique_lock<std::mutex> l(mu_);
            cv_.wait(l, [this] { return done_; });
            *stats = result_;
            return std::move(status_);
        }

     private:
        void NextWrite() {
            if (points_remaining_ != 0) {
                stream_status_ = MakeStreamStatus();
                std::cout << "Sending stream status for " << stream_status_.stream_id() << std::endl;
                StartWrite(&stream_status_);
                points_remaining_--;
            } else {
                StartWritesDone();
                RemoveHold();
            }
        }
        ClientContext context_;
        int points_remaining_ = 100;
        NotifySummary result_;
        grpc::Alarm alarm_;
        std::mutex mu_;
        std::condition_variable cv_;
        Status status_;
        StreamStatus stream_status_;
        bool done_ = false;
    };

    explicit NannyCommunicationClient(const std::string & address)
            : stub_(nullptr) {
        gpr_timespec time_spec;
        time_spec.tv_sec = 5;
        time_spec.tv_nsec = 0;
        time_spec.clock_type = GPR_TIMESPAN;

        ChannelArguments channel_args;
        channel_args.SetInt(GRPC_ARG_KEEPALIVE_TIME_MS, PING_INTERVAL);
        channel_args.SetInt(GRPC_ARG_KEEPALIVE_TIMEOUT_MS, 20000);
        channel_args.SetInt(GRPC_ARG_KEEPALIVE_PERMIT_WITHOUT_CALLS, 1);
        channel_args.SetInt(GRPC_ARG_HTTP2_MAX_PINGS_WITHOUT_DATA, 0);

        auto channel = grpc::CreateCustomChannel("localhost:50051", grpc::InsecureChannelCredentials(), channel_args);
        while (!channel->WaitForConnected(time_spec)) {
            std::cout << "grpc channel connection is failed, reconnecting." << std::endl;
            channel = grpc::CreateChannel("localhost:50051", grpc::InsecureChannelCredentials());
        }

        stub_ = NannyCommunication::NewStub(channel);
    }

    void WatchStreamConfigs() {
        ServerId id;
        id.set_id("some_id");

        Reader reader(stub_.get(), id);
        Status status = std::move(reader.Await());
        if (status.ok()) {
            std::cout << "Getting all streams rpc succeeded." << std::endl;
        } else {
            std::cout << "Getting all streams rpc failed." << std::endl;
        }
    }

    void NotifyStreamStatus() {
        Recorder recorder(stub_.get());
        NotifySummary result;
        Status status = std::move(recorder.Await(&result));
        if (status.ok()) {
            std::cout << "Finished sending stream status notifications with "
                      << result.notify_uptime() << " time elapsed\n" << std::endl;
        } else {
            std::cout << "Sending stream status notifications rpc failed." << std::endl;
        }
    }

    void LaunchBothStreamings() {
        ServerId id;
        id.set_id("some_id");

        recorder_.reset(new Recorder(stub_.get()));
        reader_.reset(new Reader(stub_.get(), id));

        std::this_thread::sleep_for(std::chrono::seconds(100));
    }

 private:
    std::shared_ptr<Recorder> recorder_;
    std::shared_ptr<Reader> reader_;
    std::unique_ptr<NannyCommunication::Stub> stub_;
};

int main(int argc, char** argv) {
    NannyCommunicationClient nanny_comm("localhost:50051");

    nanny_comm.LaunchBothStreamings();

    return 0;
}
